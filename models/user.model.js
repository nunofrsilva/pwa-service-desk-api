/**
 * User Model
 * @type {module:mongoose}
 */
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;
const CONFIG = require('../config/config');

const userSchema = new Schema({
    name: {type: String, required: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    public_key: {type: String, required: false},
    private_key: {type: String, required: false},
    role: {type: String, default: CONFIG.roles.customer},
    active: { type: Boolean, default: true}
});

userSchema
    .pre("save", function (callback) {
        this.public_key = Math.random().toString(36).substring(2) + this._id;
        this.private_key = Math.random().toString(36).substring(2) + this._id;
        callback();
    })
    .plugin(uniqueValidator);

module.exports = global.mongoConnection.model(CONFIG.mongodb.collections.user, userSchema);