/**
 * Service Request Model
 * @type {module:mongoose}
 */
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;
const CONFIG = require('../config/config');

const ServiceRequestSchema = new Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    category: {type: Schema.Types.ObjectId, ref: CONFIG.mongodb.collections.category, required: false},
    requestedBy: { type: Schema.Types.ObjectId, ref: CONFIG.mongodb.collections.user},
    assignTo: { type: Schema.Types.ObjectId, ref: CONFIG.mongodb.collections.user, required: false},
    solution: {type: String, required: false},
    state: {type: String, default: CONFIG.requestState.open},
    requestNumber: {type: String, default: 0},
    createdOn: {type: Date, default: Date.now}
});

ServiceRequestSchema.plugin(uniqueValidator).pre("save", function (callback) {
    this.state = CONFIG.requestState.open
    let date = new Date();
    this.requestNumber = date.getFullYear()+""+date.getMonth()+""+date.getDay()+""+date.getMilliseconds();
    this.createdOn = Date.now;
    callback();
});

module.exports = global.mongoConnection.model(CONFIG.mongodb.collections.service_request, ServiceRequestSchema);