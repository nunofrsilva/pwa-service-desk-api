/**
 * Category Model
 * @type {module:mongoose}
 */
const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;
const CONFIG = require('../config/config');
const categorySchema = new Schema({
    name: {type: String, required: true},
});

categorySchema.plugin(uniqueValidator);

module.exports = global.mongoConnection.model(CONFIG.mongodb.collections.category, categorySchema);