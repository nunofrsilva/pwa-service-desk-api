const express = require('express');
let router = express.Router();
const BotController = require('../controllers/bot.controller');
const CONFIG = require("../config/config");
const Auth = require('../middleware/auth');
const {
    body,
    param,
    sanitizeBody
} = require('express-validator');

router.route('/')
    .post(Auth(CONFIG.roles.toArray()),[body('message').isString()], BotController.chat);

module.exports = router;