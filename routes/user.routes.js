const express = require('express');
let router = express.Router();
const UserController = require('../controllers/user.controller');
const CONFIG = require("../config/config");
const Auth = require('../middleware/auth');
const {
    body,
    param,
    sanitizeBody
} = require('express-validator');

router.route('/')
    .post(Auth(CONFIG.roles.admin),[
        body('name').isString(),
        body('email').isEmail(),
        body('password').isString(),
        body('role').custom((value, { req }) => CONFIG.roles.toArray().includes(value)),
        sanitizeBody('name').whitelist(CONFIG.sanitize.alphabet),
    ], UserController.create)
    .get(Auth([CONFIG.roles.admin, CONFIG.roles.technical]),UserController.get);

router.route('/all-names').get(Auth(CONFIG.roles.toArray()), UserController.getAllUserNames);

router.route('/:id')
    .get(Auth(CONFIG.roles.admin), [param("id").isMongoId()], UserController.getOne)
    .put(Auth(CONFIG.roles.admin), [param("id").isMongoId()], UserController.update)
    .delete(Auth(CONFIG.roles.admin), [param("id").isMongoId()], UserController.delete)

module.exports = router;