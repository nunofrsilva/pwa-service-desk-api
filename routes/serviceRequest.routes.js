const express = require('express');
let router = express.Router();
const ServiceRequest = require('../controllers/serviceRequest.controller');
const CONFIG = require("../config/config");
const Auth = require('../middleware/auth');
const {
    body,
    param,
    sanitizeBody
} = require('express-validator');

router.route('/')
    .get(Auth(CONFIG.roles.toArray()), ServiceRequest.get)
    .post(Auth(CONFIG.roles.toArray()),[
        body('title').isString(),
        body('description').isString()
    ], ServiceRequest.create);

router.route('/:id')
    .get(Auth(CONFIG.roles.toArray()), [param("id").isMongoId()], ServiceRequest.getOne)
    .put(Auth([CONFIG.roles.admin, CONFIG.roles.technical]), [param("id").isMongoId()], ServiceRequest.update)
    .delete(Auth([CONFIG.roles.admin, CONFIG.roles.technical]), [param("id").isMongoId()], ServiceRequest.setState)

module.exports = router;