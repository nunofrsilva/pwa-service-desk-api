const express = require('express');
let router = express.Router();
const CategoryController = require('../controllers/category.controller');
const CONFIG = require("../config/config");
const Auth = require('../middleware/auth');
const {
    body,
    param,
    sanitizeBody
} = require('express-validator');

router.route('/')
    .get(Auth(CONFIG.roles.toArray()), CategoryController.get)
    .post(Auth(CONFIG.roles.admin), [body('name').isString()], CategoryController.create);

router.route('/:id')
    .get(Auth(CONFIG.roles.toArray()), [param("id").isMongoId()], CategoryController.getOne)
    .put(Auth(CONFIG.roles.admin), [param("id").isMongoId()], CategoryController.update)
    .delete(Auth(CONFIG.roles.admin), [param("id").isMongoId()], CategoryController.delete);

module.exports = router;