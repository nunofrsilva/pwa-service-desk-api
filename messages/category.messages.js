/**
 * Category Messages
 * @type {{}}
 */
module.exports = {
    success: {
        s0: {
            http: 201,
            code: "CategoryCreated",
            type: "success"
        },
        s1: {
            http: 200,
            code: "CategoryUpdated",
            type: "success"
        },
        s2: {
            http: 200,
            code: "CategoryFound",
            type: "success"
        },
        s3: {
            http: 200,
            code: "CategoryDeleted",
            type: "success"
        },
        s4: {
            http: 200,
            code: "Deactivated",
            type: "success"
        },
        s5: {
            http: 204,
            code: "NoCategories",
            type: "success"
        },
        s6: {
            http: 200,
            code: "Activated",
            type: "success"
        }
    },
    error: {
        e0: {
            http: 409,
            code: "CategoryDuplicate",
            type: "error"
        },
        e1: {
            http: 404,
            code: "CategoryNotFound",
            type: "error"
        }
    },
    invalid: {
    }
}