/**
 * Service Request Messages
 * @type {{}}
 */
module.exports = {
    success: {
        s0: {
            http: 201,
            code: "ServiceRequestCreated",
            type: "success"
        },
        s1: {
            http: 200,
            code: "ServiceRequestUpdated",
            type: "success"
        },
        s2: {
            http: 200,
            code: "ServiceRequestFound",
            type: "success"
        },
        s3: {
            http: 200,
            code: "ServiceRequestDeleted",
            type: "success"
        },
        s4: {
            http: 200,
            code: "Deactivated",
            type: "success"
        },
        s5: {
            http: 204,
            code: "NoServiceRequests",
            type: "success"
        }
    },
    error: {
        e0: {
            http: 409,
            code: "ServiceRequestDuplicate",
            type: "error"
        },
        e1: {
            http: 404,
            code: "ServiceRequestNotFound",
            type: "error"
        }
    },
    invalid: {
    }
}