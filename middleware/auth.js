const jwt = require('jsonwebtoken');
const AuthMessages = require("../messages/auth.messages");
const User = require('../models/user.model');
const CONFIG = require('../config/config');

/**
 * This auth middleware is responsible to validate user by token
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */

function isAuthorized(role) {
    return isAuthorized[role] || (isAuthorized[role] = function(req, res, next) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            if (!token) return res.status(AuthMessages.error.e1.http).send(AuthMessages.error.e1);
            const payload = jwt.decode(token);
            User.findOne({
                "public_key": payload.publicKey
            }, (error, user) => {
                if (error) throw error;
                if (!user) return res.status(AuthMessages.error.e2.http).send(AuthMessages.error.e2);
                if (Array.isArray(role)) {
                    if (!role.includes(user.role)) {
                        return res.status(AuthMessages.error.e2.http).send(AuthMessages.error.e2);
                    }
                } else if (role !== user.role) {
                    return res.status(AuthMessages.error.e2.http).send(AuthMessages.error.e2);
                }
                jwt.verify(token, user.private_key, (error) => {
                    if (error) return res.status(AuthMessages.error.e1.http).send(AuthMessages.error.e1);
                    req.user = user;
                    return next();
                });
            });
        } catch {
            res.status(401).json({
                error: new Error('Invalid request!')
            });
        }
    });
}

module.exports = isAuthorized;