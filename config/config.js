/**
 * Config File
 */

module.exports = {
    mongodb: {
        uri: 'mongodb+srv://pwaUser:pwaUserPass21@finalprojectpwa.n33lr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
        collections: {
            user: 'users',
            category: 'categories',
            service_request: 'service_requests'
        }
    },
    auth: {
        expiration_time: 15000,
        issuer: "UAB-PWA"
    },
    sanitize: {
        alphabet: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËẼÌÍÎÏĨÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëẽìíîïĩðñòóôõöøùúûüýÿ\\ ",
        numerical: "0123456789"
    },
    roles: Object.freeze({
        admin: "admin",
        technical: "technical",
        customer: "customer",
        toArray() {
            return [this.admin, this.technical, this.customer]
        }
    }),
    requestState: Object.freeze({
        open: "open",
        work_in_progress: "work_in_progress",
        close: "close",
        cancelled: "cancelled",
        toArray() {
            return [this.open, this.work_in_progress, this.close, this.cancelled]
        }
    }),
    wit: {
        accessToken: "TMCCYPGMNIGQBHKZPYED3NA57ZHCLHQE"
    }
}