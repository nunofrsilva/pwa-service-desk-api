const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const User = require('../models/user.model')
const AuthMessages = require("../messages/auth.messages");
const CONFIG = require("../config/config");
const bcrypt = require("bcrypt");
const {
    validationResult
} = require('express-validator');

/**
 * Login Method is responsible to authenticate user by email and password, and return an authentication token to use in next requests
 * @param req
 * @param res
 * @param next
 */
exports.login = (req, res, next) => {
    let email = req.body.email;
    let password = escape(req.body.password);
    User.findOne({
        "email": email
    }, (error, user) => {
        if (error) throw error;
        if (!user || !bcrypt.compareSync(password, user.password)) {
            return res.status(AuthMessages.error.e1.http).send(AuthMessages.error.e1);
        }
        const token = jwt.sign(
            {publicKey: user.public_key, userId: user._id, role: user.role},
            user.private_key,
            {expiresIn: CONFIG.auth.expiration_time, issuer: CONFIG.auth.issuer}
        );
        return res.status(AuthMessages.success.s0.http).send({
            userId: user._id,
            name: user.name,
            role: user.role,
            token: token
        });
    });
}