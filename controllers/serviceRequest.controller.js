/**
 * Service Request Controller
 * @type {{}}
 */
const ServiceRequest = require('../models/serviceRequest.model');
const jwt = require('jsonwebtoken');
const {
    validationResult
} = require('express-validator');
const SRequestMessage = require('../messages/serviceRequest.messages');
const AuthMessages = require("../messages/auth.messages");
const CONFIG = require("../config/config");

/**
 * Get all service requests
 * @param req
 * @param res
 */
exports.get = (req, res) => {
    const loggedUser = getToken(req, res);
    const query = req.query;
    if (loggedUser.role === CONFIG.roles.customer) {
        query["requestedBy"] = loggedUser.userId;
    }
    ServiceRequest.find(query, (error, serviceRequest) => {
        if (error) throw error;
        let message = SRequestMessage.success.s2;
        if (serviceRequest.length < 0)
            message = SRequestMessage.success.s5;
        message.body = serviceRequest;
        return res.status(message.http).send(message);
    });
}

/**
 * Create Service Request
 * @param req
 * @param res
 * @returns {*}
 */
exports.create = (req, res) => {
    console.log("request: ", req);
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);
    const loggedUser = getToken(req, res);
    new ServiceRequest({
        title: req.body.title,
        description: req.body.description,
        category: req.body.category,
        requestedBy: loggedUser.userId,
    }).save((error, serviceRequest) => {
        if (error) throw error;
        let message = SRequestMessage.success.s0;
        message.body = serviceRequest;
        return res.status(message.http).send(message);
    });
}

/**
 * Update Service Request
 * @param req
 * @param res
 * @returns {*}
 */
exports.update = (req, res) => {
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);
    ServiceRequest.findOneAndUpdate({
        _id: req.params.id
    }, {
        $set: req.body
    }, {
        new: true
    }, (error, serviceRequest) => {
        if (error) throw error;
        if (!serviceRequest) return res.status(SRequestMessage.error.e0.http).send(SRequestMessage.error.e0);
        let message = SRequestMessage.success.s1;
        message.body = serviceRequest;
        return res.status(message.http).send(message);
    });
}

/**
 * Get Service Request by Id
 * @param req
 * @param res
 * @returns {*}
 */
exports.getOne = (req, res) => {
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);

    ServiceRequest.findOne({
        _id: req.params.id
    }, (error, serviceRequest) => {
        if (error) throw error;
        if (!serviceRequest) return res.status(SRequestMessage.error.e1.http).send(SRequestMessage.error.e1);
        let message = SRequestMessage.success.s2;
        message.body = serviceRequest;
        return res.status(message.http).send(message);
    });
}

/**
 * Set State of service request
 * @param req
 * @param res
 * @returns {*}
 */
exports.setState = (req, res) => {
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);
    ServiceRequest.updateOne({
        _id: req.params.id
    }, {
        $set: {
            state: req.body.state
        }
    }, (error, result) => {
        if (error) throw error;
        if (result.n <= 0) return res.status(SRequestMessage.error.e1.http).send(SRequestMessage.error.e1);
        return res.status(SRequestMessage.success.s1.http).send(SRequestMessage.success.s1);
    });

}

const getToken = (req, res) => {
    const token = req.headers.authorization.split(' ')[1];
    if (!token) return res.status(AuthMessages.error.e1.http).send(AuthMessages.error.e1);
    const payload = jwt.decode(token);
    return payload;
};