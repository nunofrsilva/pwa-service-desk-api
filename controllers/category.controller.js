/**
 * Category controller
 *
 * @type {{}}
 */
const Category = require('../models/category.model');
const {
    validationResult
} = require('express-validator');
const CategoryMessages = require("../messages/category.messages");

/**
 * Get All categories
 * @param req
 * @param res
 */
exports.get = (req, res) => {
    Category.find(req.query, (error, categories) => {
        if (error) throw error;
        let message = CategoryMessages.success.s2;
        if (categories.length < 0)
            message = CategoryMessages.success.s5;
        message.body = categories;
        return res.status(message.http).send(message);
    });
}

/**
 * Create Category
 * @param req
 * @param res
 * @returns {*}
 */
exports.create = (req, res) => {
    console.log("request ", req);
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);

    new Category({
        name: req.body.name
    }).save((error, category) => {
        if (error) throw error;
        let message = CategoryMessages.success.s0;
        message.body = category;
        return res.status(message.http).send(message);
    });
}

/**
 * Update Category
 * @param req
 * @param res
 * @returns {*}
 */
exports.update = (req, res) => {
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);

    Category.findOneAndUpdate({
        _id: req.params.id
    }, {
        $set: req.body
    }, {
        new: true
    }, (error, category) => {
        if (error) throw error;
        if (!category) return res.status(CategoryMessages.error.e0.http).send(CategoryMessages.error.e0);
        let message = CategoryMessages.success.s1;
        message.body = category;
        return res.status(message.http).send(message);
    });
}

/**
 * Delete Category
 * @param req
 * @param res
 * @returns {*}
 */
exports.delete = (req, res) => {
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);

    Category.deleteOne({
        _id: req.params.id
    }, (error, result) => {
        if (error) throw error;
        if (result.deletedCount <= 0) return res.status(CategoryMessages.error.e0.http).send(CategoryMessages.error.e0);
        return res.status(CategoryMessages.success.s3.http).send(CategoryMessages.success.s3);
    });
}

/**
 * Get One Category by Id
 * @param req
 * @param res
 * @returns {*}
 */
exports.getOne = (req, res) => {
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);

    Category.findOne({
        _id: req.params.id
    }, (error, sponsor) => {
        if (error) throw error;
        if (!sponsor) return res.status(CategoryMessages.error.e1.http).send(CategoryMessages.error.e1);
        let message = CategoryMessages.success.s2;
        message.body = sponsor;
        return res.status(message.http).send(message);
    });
}