const {Wit, log} = require('node-wit');
const ServiceRequest = require('../models/serviceRequest.model');
const CONFIG = require("../config/config");
const jwt = require("jsonwebtoken");
const AuthMessages = require("../messages/auth.messages");

/**
 * Chat endpoint
 *
 * @param req
 * @param res
 */
exports.chat = (req, res) => {
    const client = new Wit({
        accessToken: CONFIG.wit.accessToken,
        logger: new log.Logger(log.DEBUG)
    });
    client.message(req.body.message, {})
        .then((data) => {
            console.log(data);
            const intent = data.intents[0].name;
            switch (intent) {
                case "greetings":
                case "bye":
                    return res.status(200).send({
                        http: 200,
                        code: "success",
                        type: "success",
                        intent: "greetings",
                        body: ""
                    });
                    break;
                case "CreateServiceRequest":
                    createService(req, res, data);
                    break;
                case "CheckStateOfService":
                    checkRequestStatus(req, res, data);
                    break;
                default:
                    return res.status(200).send({
                        http: 200,
                        code: "success",
                        type: "success",
                        intent: "undefined",
                        body: "Não foi possível processar o seu pedido!"
                    });
            }


        })
        .catch((error) => {
            console.log(error);
            return res.status(200).send({
                http: 200,
                code: "success",
                type: "error",
                intent: "undefined",
                body: "Não foi possível encontrar o seu pedido!"
            });
        });
}
/**
 * Check status of service request
 * @param req
 * @param res
 * @param data
 */
const checkRequestStatus = (req, res, data) => {
    const status = [
        { key: "open", name: "Open"},
        { key: "work_in_progress", name: "In Progress"},
        { key: "closed", name: "Closed"}
    ];
    const loggedUser = getToken(req, res);
    console.log(data.entities['serviceRequest:serviceRequest'][0].body);
    const query = {
        requestNumber: data.entities['serviceRequest:serviceRequest'][0].body
    };
    if (loggedUser.role === CONFIG.roles.customer) {
        query["requestedBy"] = loggedUser.userId;
    }
    ServiceRequest.findOne(query, (error, serviceRequest) => {
        if (error) throw error;
        let message = {
            http: 200,
            code: "success",
            type: "success",
            intent: "checkService"
        };
        console.log(serviceRequest);
        if (!serviceRequest) {
            message.body = "Não foi possível encontrar um pedido registado com o número "+query.requestNumber+". Por favor verifique se o mesmo está correto!";
        } else {
            message.body = "O seu pedido número "+ serviceRequest.requestNumber + " encontra-se no estado "+ status.find(s => s.key === serviceRequest.state).name;
        }
        return res.status(200).send(message);
    });
}

/**
 * Create service Request
 * @param req
 * @param res
 * @param data
 */
const createService = (req, res, data) => {
    const description = data.entities['wit$message_body:message_body'][0].body
    const loggedUser = getToken(req, res);
    new ServiceRequest({
        title: description.substring(0,200),
        description: data.text,
        requestedBy: loggedUser.userId,
    }).save((error, serviceRequest) => {

        let message = {
            http: 200,
            code: "success",
            type: "success",
            intent: "createService"
        };
        if (error || !serviceRequest) {
          message.body = "Não foi possível registar o seu pedido, por favor tente mais tarde!";
        } else {
            message.body = `O seu pedido foi registado com número ${serviceRequest.requestNumber}` ;
        }
        return res.status(message.http).send(message);
    });
}

/**
 * Get user token
 * @param req
 * @param res
 * @returns {{payload: *, signature: *, header: *}|*}
 */
const getToken = (req, res) => {
    const token = req.headers.authorization.split(' ')[1];
    if (!token) return res.status(AuthMessages.error.e1.http).send(AuthMessages.error.e1);
    return jwt.decode(token);
};