/**
 * User Controller
 * @type {{}}
 */
const User = require('../models/user.model');
const {
    validationResult
} = require('express-validator');
const UserMessages = require("../messages/user.messages");
const bcrypt = require("bcrypt");

/**
 * Get all users
 * @param req
 * @param res
 */
exports.get = (req, res) => {
    User.find(req.query, (error, users) => {
        if (error) throw error;
        let message = UserMessages.success.s2;
        if (users.length < 0)
            message = UserMessages.success.s5;
        message.body = users;
        return res.status(message.http).send(message);
    });
}
/**
 * Get User by Id
 * @param req
 * @param res
 * @returns {*}
 */
exports.getOne = (req, res) => {
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);

    User.findOne({
        _id: req.params.id
    }, (error, user) => {
        if (error) throw error;
        if (!user) return res.status(UserMessages.error.e1.http).send(UserMessages.error.e1);
        let message = UserMessages.success.s2;
        message.body = user;
        return res.status(message.http).send(message);
    });
};

/**
 * Create User
 *
 * @param req
 * @param res
 * @returns {*}
 */
exports.create = (req, res) => {
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);

    User.findOne({
        "email": req.body.email
    }, (error, user) => {
        if (error) throw error;
        if (user) return res.status(UserMessages.error.e0.http).send(UserMessages.error.e0)
        bcrypt.hash(req. body.password, 10).then(
            (hash) => {
                const user = new User({
                    name: req.body.name,
                    email: req.body.email,
                    password: hash,
                    role: req.body.role,
                }).save((error, user) => {
                    if (error) throw error;
                    return res.status(UserMessages.success.s0.http).send(user);
                });
            }
        );
    });
}

/**
 * Update User
 *
 * @param req
 * @param res
 * @returns {*}
 */
exports.update = (req, res) => {
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);

    User.findOneAndUpdate({
        _id: req.params.id
    }, {
        $set: req.body
    }, {
        new: true
    }, (error, user) => {
        if (error) throw error;
        if (!user) return res.status(UserMessages.error.e1.http).send(UserMessages.error.e1);

        let message = UserMessages.success.s1;
        message.body = user;
        return res.status(message.http).send(message);

    });
}

/**
 * Delete User
 *
 * @param req
 * @param res
 * @returns {*}
 */
exports.delete = (req, res) => {
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);
    User.deleteOne({
        _id: req.params.id
    }, (error, result) => {
        if (error) throw error;
        if (result.deletedCount <= 0) return res.status(UserMessages.error.e1.http).send(UserMessages.error.e1);
        return res.status(UserMessages.success.s3.http).send(UserMessages.success.s3);
    });

}

/**
 * Active User
 * @param req
 * @param res
 * @returns {*}
 */
exports.activate = (req, res) => {
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);

    User.updateOne({
        _id: req.params.id
    }, {
        $set: {
            active: true
        }
    }, (error, result) => {
        if (error) throw error;

        if (result.n <= 0) return res.status(UserMessages.error.e0.http).send(UserMessages.error.e0);
        return res.status(UserMessages.success.s6.http).send(UserMessages.success.s6);

    });
}

/**
 * Disable user
 * @param req
 * @param res
 * @returns {*}
 */
exports.deactivate = (req, res) => {
    const errors = validationResult(req).array();
    if (errors.length > 0) return res.status(406).send(errors);

    User.updateOne({
        _id: req.params.id
    }, {
        $set: {
            active: false
        }
    }, (error, result) => {
        if (error) throw error;

        if (result.n <= 0) return res.status(UserMessages.error.e0.http).send(UserMessages.error.e0);
        return res.status(UserMessages.success.s4.http).send(UserMessages.success.s4);

    });
}

exports.getAllUserNames = (req, res) => {
    User.find(req.query, (error, users) => {
        if (error) throw error;
        let message = UserMessages.success.s2;
        if (users.length < 0)
            message = UserMessages.success.s5;

        const listUsers = users.map(u => {
            return {
                userId: u._id,
                name: u.name
            }
        })

        message.body = listUsers;
        return res.status(message.http).send(message);
    });
}