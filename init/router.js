/**
 * Define routes
 *
 * @param app
 */
module.exports = (app) => {
    app.use('/', require("../routes/index"));
    app.use('/auth', require('../routes/auth.routes'));
    app.use('/users', require('../routes/user.routes'));
    app.use('/categories', require('../routes/category.routes'));
    app.use('/service-request', require('../routes/serviceRequest.routes'));
    app.use('/chat', require('../routes/bot.routes'));
}